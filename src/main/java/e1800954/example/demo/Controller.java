package e1800954.example.demo;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class Controller
{

    @GetMapping
    public JSONArray test() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id",1);
        JSONArray array = new JSONArray();
        array.add(jsonObject);
        return array;
    }
}
